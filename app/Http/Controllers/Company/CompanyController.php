<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\StoreRequest;
use App\Http\Requests\Company\UpdateRequest;
use App\Imports\CompanyImport;
use App\Services\CompanyService;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $companyService;

    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }

    public function index(Request $request)
    {
        try {
            $results = $this->companyService->getAll($request);

            return view('dashboard.company.index', compact('results'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('dashboard.company.create');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $this->companyService->store($request);

            return redirect()->route('company')->with('success', 'Data Created');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $result = $this->companyService->getOne($id);

            $logo = base64_encode(file_get_contents($result->logo));

            $layout = view('dashboard.company.show', compact('result', 'logo'));

            $pdf = PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif'])->loadHTML($layout);

            return $pdf->stream();
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $result = $this->companyService->getOne($id);

            return view('dashboard.company.edit', compact('result'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $this->companyService->update($request, $this->companyService->getOne($id));

            DB::commit();

            return redirect()->route('company')->with('success', 'Data diubah');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->companyService->destroy($this->companyService->getOne($id));

            return redirect()->route('company')->with('success', 'Data dihapus');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
