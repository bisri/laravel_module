@extends('home')

@section('main-content')

<form action="{{route('employee.store')}}" method="POST">
    @csrf
    @method('POST')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="modal-title-default">Create Employee</h6>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">Nama</label>
                    <input type="text" class="form-control" id="input-name" name="nama" value="{{ old('nama') }}" placeholder="Nama">
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label class="form-control-label" for="input-email">Email</label>
                    <input type="email" class="form-control" id="input-email" name="email" value="{{ old('email') }}" placeholder="Email">
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label class="form-control-label" for="input-company">Company</label>
                    <select class="form-control js-example-basic-single" id="company" name="company_id">
                        
                    </select>
                </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
        </div>
    </div>
</form>
@endsection

@section('custom-js')
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2({
            ajax: {
                url: '/employee/companies',
                dataType: 'json',
                method: 'GET',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                }
            }
        });
    });
</script>
@endsection