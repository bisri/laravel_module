<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Transisi\Http\Requests\Employee\StoreRequest;
use Modules\Transisi\Http\Requests\Employee\UpdateRequest;
use Modules\Transisi\Repositories\CompanyRepository;
use Modules\Transisi\Repositories\EmployeeRepository;
use Modules\Transisi\Imports\EmployeesImport;

class EmployeeController extends Controller
{
    protected $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index(Request $request)
    {
        try {
            $results = $this->employeeRepository->fetch($request);

            return view('transisi::dashboard.employee.index', compact('results'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    public function getCompany(Request $request, CompanyRepository $companyRepository)
    {
        try {
            $results = $companyRepository->fetchSelect2($request);

            return response()->json($results);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('transisi::dashboard.employee.create');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $this->employeeRepository->store($request);

            return redirect()->route('employee')->with('Success', 'Data Ditambahkan');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function import(Request $request)
    {
        try {
            Excel::import(new EmployeesImport(), $request->file('excel'), 's3', \Maatwebsite\Excel\Excel::XLSX);

            return redirect()->route('employee')->with('success', 'Data Imported');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $result = $this->employeeRepository->getOne($id);

            return view('transisi::dashboard.employee.edit', compact('result'));
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $this->employeeRepository->update($request, $this->employeeRepository->getOne($id));

            return redirect()->route('employee')->with('success', 'Data dibuat');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->employeeRepository->destroy($this->employeeRepository->getOne($id));

            return redirect()->route('employee')->with('success', 'Data dihapus');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
            abort(500);
        }
    }
}
