<?php

namespace Modules\Transisi\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Transisi\Http\Requests\Company\StoreRequest;
use Modules\Transisi\Http\Requests\Company\UpdateRequest;
use Modules\Transisi\Repositories\CompanyRepository;

class CompanyController extends Controller
{
    protected $repository;

    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        try {
            $results = $this->repository->fetch($request);

            return response()->json($results);
        } catch (\Throwable $th) {
            Log::error("Controller Error " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(StoreRequest $request)
    {
        try {
            $path = '';

            if ($request->file('logo')) {
                $path = uploadFile($request->file('logo'), 'company');
            }

            $request->path = $path;

            $result = $this->repository->store($request);

            return response()->json($result);
        } catch (\Throwable $th) {
            Log::error("Controller Error " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        try {
            $result = $this->repository->find($id);

            return response()->json($result);
        } catch (\Throwable $th) {
            Log::error("Controller Error " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $result = $this->repository->update($request, $this->repository->find($id));

            return response()->json($result);
        } catch (\Throwable $th) {
            Log::error("Controller Error " . $th->getMessage());
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $result = $this->repository->destroy($this->repository->find($id));

            return response()->json($result);
        } catch (\Throwable $th) {
            Log::error("Controller Error " . $th->getMessage());
            abort(500);
        }
    }
}
