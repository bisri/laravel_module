<?php

namespace Modules\Transisi\Repositories;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmployeeRepository
{
    public function getOne($id)
    {
        try {
            $result = Employee::with('company')->findOrFail($id);

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function fetch($request)
    {
        try {
            $results = Employee::when($request->text_search, function ($query) use ($request) {
                $query->where('nama', 'like', '%' . $request->text_search . '%');
            })
                     ->orderBy('id', 'desc')
                     ->paginate(5);

            return $results;
        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function import($request)
    {
        try {
            $result = Employee::create([
                'nama' => $request[0],
                'email' => $request[1],
                'company_id' => $request[2]
            ]);

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try {
            $result = Employee::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'company_id' => $request->company_id
            ]);

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Employee $employee)
    {
        try {
            $result = $employee->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'company_id' => $request->company_id,
            ]);

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Employee $employee)
    {
        try {
            $result = $employee->delete();

            return $result;
        } catch (\Throwable $th) {
            Log::error("Service error. " . $th->getMessage());
            abort(500);
        }
    }
}
