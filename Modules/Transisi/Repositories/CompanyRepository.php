<?php

namespace Modules\Transisi\Repositories;

use App\Models\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CompanyRepository
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function fetchSelect2($request)
    {
        $term = trim($request->term);
        $companies = Company::when($term, function ($query) use ($request) {
            $query->where('nama', 'like', '%' . trim($request->term) . '%');
        })
            ->orderBy('id', 'desc')
            ->paginate(5);

        $morePages=true;
        $pagination_obj= json_encode($companies);
        if (empty($companies->nextPageUrl())) {
            $morePages=false;
        }
        $results = array(
                "results" => $companies->getCollection()->transform(function ($company) {
                    return $this->select2Format($company);
                }),
                "pagination" => array(
                "more" => $morePages
                )
            );

        return $results;
    }


    public function fetch($request)
    {
        $query = $this->model->query();

        if (isset($request->status)) {
            $query->where('status', $request->status);
        }

        return $query = $query->paginate();
    }

    public function store($request)
    {
        try {
            $result = Company::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'logo' => $request->path,
                'website' => $request->website,
            ]);

            return $result;
        } catch (\Throwable $th) {
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Company $company)
    {
        $path = $company->logo;

        if ($request->file('logo')) {
            $path = uploadFile($request->file('logo'), 'company');
        }

        $result = $company->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'logo' => $path,
            'website' => $request->website,
        ]);

        return $result;
    }

    public function destroy(Company $company)
    {
        deleteFile($company->logo);

        $result = $company->delete();

        return $result;
    }

    public function select2Format($company)
    {
        return [
            'id' => $company->id,
            'text' => $company->nama
        ];
    }
}
