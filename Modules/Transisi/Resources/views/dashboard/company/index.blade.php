@extends('home')

@section('main-content')
<div class="card">
    <div class="card-header">
        <h6>Company</h6>
        
    </div>
    <div class="col" style="margin-top: 5px;">
        <a class="btn btn-primary" href="{{route('company.create')}}">Create</a>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Logo</th>
                <th scope="col">Website</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                @foreach ($results as $result)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$result->nama}}</td>
                        <td>{{$result->email}}</td>
                        <td>
                          <img src="{{asset($result->logo)}}" width="100">
                        </td>
                        <td>{{$result->website}}</td>
                        <td>
                          <form action="{{ route('company.destroy', $result->id) }}" method="post" id="form-{{ $result->id }}">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-success btn-sm text-white" href="{{route('company.edit', $result->id)}}">
                              Edit
                            </a>
                            <a class="btn btn-primary btn-sm text-white" href="{{route('company.show', $result->id)}}">
                              Pdf
                            </a>
                            <button type="submit" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $result->id }}">
                              Delete
                            </button>
                          </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-6 col-sm-12 text-sm">
          Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} result
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="float-right">
            {{ $results->links() }}
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

{{-- @section('custom-js')
<script>
  $(document).ready(()=>{
    $('.btn-hapus').on('click', function (e) {
      console.log('haha')
      let id = e.target.dataset['id']

      Swal.fire({
        title: 'Warning',
        text: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
      }).then((data) => {
        if (data.isConfirmed) {
          $(`#form-${id}`).submit()
        }
      })
    })
  })
  
</script>
@endsection --}}