<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Transisi\Http\Controllers\EmployeeController;

Route::group(['prefix' => 'transisi', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', 'TransisiController@index');

    Route::group(['prefix' => 'employee'], function () {
        Route::get('/', [EmployeeController::class, 'index'])->name('employee');
        Route::get('/companies', [EmployeeController::class, 'getCompany'])->name('employee.getCompany');
        Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');
        Route::post('/import', [EmployeeController::class, 'import'])->name('employee.import');
        Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
        Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
        Route::put('/update/{id}', [EmployeeController::class, 'update'])->name('employee.update');
        Route::delete('/{id}', [EmployeeController::class, 'destroy'])->name('employee.destroy');
    });
});
