<?php

namespace Modules\Transisi\Constants;

class Status
{
    public const ACTIVE = 1;
    public const INACTIVE = 2;
    public const BLOCK = 3;

    public static function labels(): array
    {
        return [
            self::ACTIVE => "Aktif",
            self::INACTIVE => "Tidak Aktif",
            self::BLOCK => "Di Block",
        ];
    }
}
