<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Transisi',
            'email' => 'admin@transisi.id',
            'password' => Hash::make('transisi')
        ]);

        $admin->assignRole('admin');

        $user = User::create([
            'name' => 'Bisri',
            'email' => 'bisri@transisi.id',
            'password' => Hash::make('transisi')
        ]);

        $user->assignRole('user');
    }
}
