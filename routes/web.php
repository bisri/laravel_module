<?php

use App\Http\Controllers\Company\CompanyController;
use App\Http\Controllers\Emploeyee\EmployeeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes([
	'register' => false,
	'verify' => true
]);

Route::group(['middleware' => ['auth', 'role:admin']], function() {

    /**
     * MANAGE COMPANY
     */
    Route::group(['prefix' => 'company'], function(){
        Route::get('/', [CompanyController::class, 'index'])->name('company');
        Route::get('/{id}/show', [CompanyController::class, 'show'])->name('company.show');
        Route::get('/create', [CompanyController::class, 'create'])->name('company.create');
        Route::post('/store', [CompanyController::class, 'store'])->name('company.store');
        Route::get('/edit/{id}', [CompanyController::class, 'edit'])->name('company.edit');
        Route::put('/update/{id}', [CompanyController::class, 'update'])->name('company.update');
        Route::delete('/{id}', [CompanyController::class, 'destroy'])->name('company.destroy');
    });

    /**
     * MANAGE EMPLOYEE
     */
    Route::group(['prefix' => 'employee'], function(){
        Route::get('/', [EmployeeController::class, 'index'])->name('employee');
        Route::get('/companies', [EmployeeController::class, 'getCompany'])->name('employee.getCompany');
        Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');
        Route::post('/import', [EmployeeController::class, 'import'])->name('employee.import');
        Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');
        Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
        Route::put('/update/{id}', [EmployeeController::class, 'update'])->name('employee.update');
        Route::delete('/{id}', [EmployeeController::class, 'destroy'])->name('employee.destroy');
    });

});

    
